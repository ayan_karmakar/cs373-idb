#!/usr/bin/env python3

from flask import Flask, render_template, send_from_directory
from flask_cors import CORS

import api

#app = Flask(__name__)
from app.models import app
CORS(app)
#@app.route('/')

@app.route('/')
@app.route('/players')
@app.route('/teams')
@app.route('/games')
@app.route('/about')
@app.route('/flights')
def serve():
    """serves React App"""
    return send_from_directory(app.static_folder,"index.html")
    #return render_template("index.html")
    
#-----------------
# API routes
#-----------------

#prefix for all api routes
api_prefix = '/api/'

@app.route(api_prefix + 'players')
def api_players():
    """Returns a list of ids for players in the NBA"""
    return api.players()

@app.route(api_prefix + 'players/page/<int:page_num>')
def api_players_by_page(page_num):
    """Returns a page of player stats"""
    return api.players_by_page(page_num)

@app.route(api_prefix + 'players/id/<int:player_id>', methods=['GET'])
def api_player_by_id(player_id):
    """Returns stats for the player with the given id"""
    return api.player_by_id(player_id)

@app.route(api_prefix + 'teams')
def api_teams():
    """Returns stats for all the teams in the NBA"""
    return api.teams()

@app.route(api_prefix + 'teams/id/<int:team_id>')
def api_team_by_id(team_id):
    """Returns stats for the team with the given id"""
    return api.team_by_id(team_id)

@app.route(api_prefix + 'teams/logos/<int:team_id>')
def api_team_logo(team_id):
    """Returns the logo of the team with a given id"""
    return api.team_logo(team_id)

@app.route(api_prefix + 'games')
def api_games():
    """Returns a list of ids for games in the NBA"""
    return api.games()

@app.route(api_prefix + 'games/page/<int:page_num>')
def api_games_by_page(page_num):
    """Returns a page of game stats"""
    return api.games_by_page(page_num)

@app.route(api_prefix + 'games/id/<int:game_id>')
def api_game_by_id(game_id):
    """Returns stats for the game with the given id"""
    return api.game_by_id(game_id)

@app.route(api_prefix + 'all/players')
def api_all_players():
    """Returns stats for all players"""
    return api.all_players()

@app.route(api_prefix + 'all/teams')
def api_all_teams():
    """Returns stats for all teams"""
    return api.all_teams()

@app.route(api_prefix + 'all/games')
def api_all_games():
    """Returns stats for all games"""
    return api.all_games()

@app.route(api_prefix + 'tests')
def api_tests():
    """Returns unit tests"""
    return api.tests()

if __name__ == "__main__":
    app.run()
