import unittest
from .models import db, Player, Team, Game

result = []

class ModelTests(unittest.TestCase):
    
    def test_1(self):
        player = db.session.query(Player).get(485)

        allAttributes = (player.player_id and player.name and player.teamID and player.position and player.height_feet 
         and player.height_inches and player.weight and player.games and player.points and player.assists and player.rebounds
         and player.steals and player.blocks)

        self.assertTrue(allAttributes)
        ans = allAttributes != 0
        res = "Player's attributes are all present: %s" % ans
        result.append(res)

    def test_2(self):
        player = db.session.query(Player).get(485)

        player_id = isinstance(player.player_id, int)
        name = isinstance(player.name, str)
        teamID = isinstance(player.teamID, int) 
        position = isinstance(player.position, str)
        height_feet = isinstance(player.height_feet, int)
        height_inches = isinstance(player.height_inches, int)
        weight = isinstance(player.weight, int)
        games = isinstance(player.games, int)
        points = isinstance(player.points, float)
        assists = isinstance(player.assists, float)
        rebounds = isinstance(player.rebounds, float)
        steals = isinstance(player.steals, float) 
        blocks = isinstance(player.blocks, float) 

        correctType = (player_id and name and teamID and position and height_feet and height_inches and weight 
        and games and points and assists and rebounds and steals and blocks)

        self.assertTrue(correctType)
        res = "Player's attributes are the correct type: %s" % correctType
        result.append(res)

    # testing creating and deleting player
    def test_3(self):
        player_id = -100
        name = 'name'
        teamID = 1
        position = 'C'
        height_feet = 6
        height_inches = 8
        weight = 250
        games = 10
        points = 10
        assists = 11.5
        rebounds = 12.5
        steals = 13.5 
        blocks = 14.5 

        newPlayer = Player( player_id = player_id, name = name, teamID = teamID, position = position, height_feet = height_feet, height_inches = height_inches, weight = weight, games = games, points = points, assists = assists, rebounds = rebounds, steals = steals, blocks = blocks)
        db.session.add(newPlayer)
        db.session.commit()

        player = db.session.query(Player).filter_by(player_id = '-100').one()
        self.assertEqual(str(player.player_id), '-100')

        ans = player.player_id == -100
        res = "Created and deleted correct player_id: %s" % ans
        result.append(res)

        db.session.query(Player).filter_by(player_id = '-100').delete()
        db.session.commit()

    # testing all team's attributes
    def test_4(self):
        team = db.session.query(Team).get(1)
        allAttributes = (team.team_id and team.name and team.city and team.abbr and team.division and team.conference)
        self.assertTrue(allAttributes)
        ans = allAttributes != 0
        res = "Team's attributes present: %s" % ans
        result.append(res)

    def test_5(self):
        team = db.session.query(Team).get(1)
        team_id = isinstance(team.team_id, int)
        name = isinstance(team.name, str)
        city = isinstance(team.city, str)
        abbr = isinstance(team.abbr, str)
        division = isinstance(team.division, str)
        conference = isinstance(team.conference, str)

        correctType = (team and team_id and name and city and abbr and division and conference)

        self.assertTrue(correctType)
        res = "Team's attributes are the correct type: %s" % correctType
        result.append(res)

    def test_6(self):
        team_id = 100
        name = 'name'
        city = 'city'
        abbr = 'abbr'
        division = 'division'
        conference = 'conference'

        newTeam = Team( team_id = team_id, name = name, city = city, abbr = abbr, division = division, conference = conference)
        db.session.add(newTeam)
        db.session.commit()

        team = db.session.query(Team).filter_by(team_id = '100').one()
        self.assertEqual(str(team.team_id), '100')

        ans = team.team_id == 100
        res = "Created and deleted correct team_id: %s" % ans
        result.append(res)

        db.session.query(Team).filter_by(team_id = '100').delete()
        db.session.commit()

    def test_7(self):
        game = db.session.query(Game).get(62585)

        allAttributes = (game.game_id and game.date and game.home_score and game.visitor_score and game.homeID 
         and game.visitorID)

        self.assertTrue(allAttributes)
        ans = allAttributes != 0
        res = "Game's attributes present: %s" % ans
        result.append(res)

    def test_8(self):
        game = db.session.query(Game).get(62585)

        game_id = isinstance(game.game_id, int)
        date = isinstance(game.date, str)
        home_score = isinstance(game.home_score, int)
        visitor_score = isinstance(game.visitor_score, int)
        homeID = isinstance(game.homeID, int)
        visitorID = isinstance(game.visitorID, int)

        correctType = (game_id and date and home_score and visitor_score and homeID and visitorID)

        self.assertTrue(correctType)
        res = "Game's attributes are the correct type: %s" % correctType
        result.append(res)

    def test_9(self):
        game_id = -100
        date = 'date'
        home_score = 0
        visitor_score = 100
        homeID = -100
        visitorID = -200

        newGame = Game(game_id = game_id, date = date, homeID = homeID, visitorID = visitorID, home_score = home_score, visitor_score = visitor_score)
        db.session.add(newGame)
        db.session.commit()

        game = db.session.query(Game).filter_by(game_id = '-100').one()
        self.assertEqual(str(game.game_id), '-100')

        ans = game.game_id == -100
        res = "Created and deleted correct game_id: %s" % ans
        result.append(res)

        db.session.query(Game).filter_by(game_id = '-100').delete()
        db.session.commit()


if __name__ == '__main__':
    unittest.main()
