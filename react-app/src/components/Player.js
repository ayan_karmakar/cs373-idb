import React from 'react';
import { Link } from 'react-router-dom';
import '../css/Player.css';
import axios from 'axios';
import url from '../url.js';

export default class Player extends React.Component {
  state = {
    data: [],
  };
  async componentDidMount() {
    let res = await axios.get(
      url + `api/players/id/${this.props.location.playerId}`
    );
    this.setState({ data: res.data });
  }

  render(
    {
      name,
      position,
      height,
      weight,
      gamesplayed,
      pointspg,
      reboundspg,
      assistspg,
      stealspg,
      blockspg,
      teamId,
    } = this.state.data
  ) {
    return (
      <div style={{ color: 'white' }}>
        {/******************** PLAYER DATA ********************/}
        <Link className='btn btn-dark back-button-custom' to='/players'>
          &laquo; Back to Players
        </Link>
        <div className='container'>
          <div className='card-deck text-center content-custom'>
            <div className='container'>
              <div className='row'>
                <div className='card mb-4 shadow-sm col-sm-7 card-header-custom'>
                  <div className='card-header card-header-text-custom'>
                    <h4>{name}</h4>
                  </div>
                  <div className='card-body player-custom'>
                    <ul className='list-unstyled mt-3 mb-4 attribute-list-custom'>
                      <li>
                        <b>Team: </b>
                        <Link
                          to={{
                            pathname: '/team',
                            teamId: teamId,
                          }}
                        >
                          {this.props.location.teamName}
                        </Link>
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li>
                        <b>Position: </b>
                        {position}
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li>
                        <b>Height (ft-in): </b>
                        {height}
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li>
                        <b>Weight (lb): </b>
                        {weight}
                      </li>
                    </ul>
                  </div>
                </div>
                {/******************** GAMES LINK ********************/}
                <div className='right column col-sm-5'>
                  <div className='card mb-5 shadow-sm games-link-custom'>
                    <Link
                      to={{
                        pathname: '/games',
                        teamId: teamId,
                      }}
                    >
                      Past Games
                    </Link>
                  </div>
                  {/******************** PLAYER STATS ********************/}
                  <div className='card mb-5 shadow-sm stats-list-custom'>
                    <div className='card-header stats-header-text-custom'>
                      <h4>Player Stats</h4>
                    </div>
                    <ul className='list-unstyled mt-3 mb-4'>
                      <li>
                        <b>Games Played: </b>
                        {gamesplayed}
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li>
                        <b>Points Per Game: </b>
                        {pointspg}
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li>
                        <b>Rebounds Per Game: </b>
                        {reboundspg}
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li>
                        <b>Assists Per Game: </b>
                        {assistspg}
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li>
                        <b>Steals Per Game: </b>
                        {stealspg}
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li>
                        <b>Blocks Per Game: </b>
                        {blockspg}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

//export default Player;
