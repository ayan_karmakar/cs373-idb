import React from 'react';
import { Link } from 'react-router-dom';

import '../css/Navbar.css';

const Navbar = () => {
  return (
    <nav
      className='navbar navbar-expand-lg'
      style={{ backgroundColor: 'black' }}
    >
      <Link className='navbar-brand nav-title-custom' to='/'>
        The Hoop Scoop
      </Link>

      <button
        className='navbar-toggler bg-dark'
        type='button'
        data-toggle='collapse'
        data-target='#navbarSupportedContent'
        aria-controls='navbarSupportedContent'
        aria-expanded='false'
        aria-label='Toggle navigation'
      >
        <span className='navbar-toggler-icon'></span>
      </button>

      <div className='collapse navbar-collapse' id='navbarSupportedContent'>
        <ul className='navbar-nav ml-md-auto'>
          <li className='nav-item my-2 active'>
            <Link className='nav-link nav-link-custom' to='/'>
              Home
            </Link>
          </li>
          <li className='nav-item my-2 active'>
            <Link className='nav-link nav-link-custom' to='/players'>
              Players
            </Link>
          </li>
          <li className='nav-item my-2 active'>
            <Link className='nav-link nav-link-custom' to='/teams'>
              Teams
            </Link>
          </li>
          <li className='nav-item my-2 active'>
            <Link className='nav-link nav-link-custom' to='/games'>
              Games
            </Link>
          </li>
          <li className='nav-item my-2 active'>
            <Link className='nav-link nav-link-custom' to='/flights'>
              Flights
            </Link>
          </li>
          <li className='nav-item my-2 active'>
            <Link className='nav-link nav-link-custom' to='/search'>
              Search
            </Link>
          </li>
          <li className='nav-item my-2 active' style={{ marginRight: '20px' }}>
            <Link className='nav-link nav-link-custom' to='/about'>
              About
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};
export default Navbar;
