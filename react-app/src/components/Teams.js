import React from 'react';
import { Link } from 'react-router-dom';
import '../css/Models.css';
import teamsPic from '../images/teams-page-picture.jpg';
import axios from 'axios';
import url from '../url.js';

export default class Teams extends React.Component {
  state = {
    data: [],
    team_list: [],
    done: false,
    nameSort: false,
    abbrSort: false,
    conferenceSort: false,
    divisionSort: false,
    citySort: false,
    nameArrow: '-',
    abbrArrow: '-',
    conferenceArrow: '-',
    divisionArrow: '-',
    cityArrow: '-',
    highlight: ''
  };

  async componentDidMount() {
    let res = await axios.get(url + 'api/all/teams');
    this.setState({ data: res.data, team_list: res.data });
    this.setState({ done: true });
  }

  onSort(event, sortKey) {
    const data = this.state.data;
    let keyName = sortKey + 'Sort';
    let keyArrow = sortKey + 'Arrow';
    let newArrow = '\u2227';
    if (this.state[keyName]) {
      data.sort((a, b) => b[sortKey].localeCompare(a[sortKey]));
      newArrow = '\u2228';
    } else {
      data.sort((a, b) => a[sortKey].localeCompare(b[sortKey]));
    }

    this.setState({
      data,
      [keyName]: !this.state[keyName],
      nameArrow: '-',
      abbrArrow: '-',
      conferenceArrow: '-',
      divisionArrow: '-',
      cityArrow: '-',
      [keyArrow]: newArrow,
    });
  }

  async handleSearch(e) {
    let text = '';
    if (typeof e === 'string') {
      this.props.location.searchText = null;
      text = e;
    } else text = e.target.value;
    this.setState({ highlight: text });
    let tlist = this.state.team_list;
    let searchData = await tlist.filter((team) =>
      team.name.toLowerCase().includes(text.toLowerCase()) ||
      team.abbr.toLowerCase().includes(text.toLowerCase()) ||
      team.conference.toLowerCase().includes(text.toLowerCase()) ||
      team.division.toLowerCase().includes(text.toLowerCase()) ||
      team.city.toLowerCase().includes(text.toLowerCase())
    );
    this.setState({ data: searchData });
  }

  displaySearch(display) {
    if (!display) return <div></div>;
    else
      return (
        <div className='search-items'>
          <label htmlFor='search' className='search-label'>
            Search:
          </label>
          <input
            className='search-input'
            name='search'
            type='text'
            autoComplete='off'
            onChange={(e) => {
              this.handleSearch(e);
            }}
          />
        </div>
      );
  }

  render() {
    if (!this.state.done) {
      return <div></div>;
    }
    var Highlight = require('react-highlighter');
    if (this.props.location.searchText)

      this.handleSearch(this.props.location.searchText);
      //console.log(this.state.highlight);
      //console.log('Cleveland'.replace(new RegExp(this.state.highlight, "i"), (match) => (<mark>{match.toString()}</mark>)));
    return (
      <div>
        {/******************** TEAM TABLE ********************/}
        <div className='container model-container-custom'>
          <div className='headers-custom'>
            <h2>NBA Teams</h2>
            {this.displaySearch(!this.props.location.displaySearch)}
          </div>
          <div className='table-responsive text-center'>
            <table
              className='table table-dark table-striped table-sm'
              style={{ backgroundColor: '#111' }}
            >
              <thead>
                <tr>
                  <th>
                    Name
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'name')}
                    >
                      {this.state.nameArrow}
                    </button>
                  </th>
                  <th>
                    Abbreviation
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'abbr')}
                    >
                      {this.state.abbrArrow}
                    </button>
                  </th>
                  <th>
                    Conference
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'conference')}
                    >
                      {this.state.conferenceArrow}
                    </button>
                  </th>
                  <th>
                    Division
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'division')}
                    >
                      {this.state.divisionArrow}
                    </button>
                  </th>
                  <th>
                    City
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'city')}
                    >
                      {this.state.cityArrow}
                    </button>
                  </th>
                </tr>
              </thead>

              <tbody>
                {this.state.data.map((team) => (
                  <tr>
                    <td>
                      <Link
                        to={{
                          pathname: '/team',
                          teamId: team.id,
                        }}
                      >
                        <Highlight search={this.state.highlight}>{team.name}</Highlight>
                      </Link>
                    </td>
                    <td><Highlight search={this.state.highlight}>{team.abbr}</Highlight></td>
                    <td><Highlight search={this.state.highlight}>{team.conference}</Highlight></td>
                    <td><Highlight search={this.state.highlight}>{team.division}</Highlight></td>
                    <td><Highlight search={this.state.highlight}>{team.city}</Highlight></td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          <img src={teamsPic} alt='nba teams' />
        </div>
      </div>
    );
  }
}
