import React from 'react';
import { Link } from 'react-router-dom';

import '../css/Home.css';

const Home = () => {
  return (
    <div>
      {/******************** JUMBOTRON ********************/}
      <div className='jumbotron jumbotron-fluid jumbo-custom'>
        <div className='container'>
          <h1 className='display-3 jumbo-title-custom'>
            <b>Welcome to the Hoop Scoop!</b>
          </h1>
          <div className='row'>
            <div className='col-sm-4'></div>
            <Link
              className='btn btn-dark btn-lg col-sm-3 jumbo-button-custom'
              to='/about'
              role='button'
            >
              Learn More
            </Link>
          </div>
        </div>
      </div>
      {/******************** SECTION CARDS ********************/}
      <div className='section-cards container'>
        <div className='card-deck text-center card-deck-custom'>
          <Link
            className='card mb-5 shadow-sm bg-dark card-custom'
            to='/players'
          >
            <div className='card-header card-header-custom'>
              <h4 className='card-header-text-custom'>Explore Players</h4>
            </div>
            <div className='card-body player-body-custom'></div>
          </Link>
          <Link className='card mb-5 shadow-sm bg-dark card-custom' to='/teams'>
            <div className='card-header card-header-custom'>
              <h4 className='card-header-text-custom'>Explore Teams</h4>
            </div>
            <div className='card-body team-body-custom'></div>
          </Link>
          <Link className='card mb-5 shadow-sm bg-dark card-custom' to='/games'>
            <div className='card-header card-header-custom'>
              <h4 className='card-header-text-custom'>Explore Games</h4>
            </div>
            <div className='card-body game-body-custom'></div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Home;
